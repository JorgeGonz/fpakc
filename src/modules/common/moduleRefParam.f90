!Reference parameters
MODULE moduleRefParam
  !Parameters that define the problem (inputs)
  REAL(8):: n_ref, m_ref, T_ref, r_ref, debye_ref, sigmaVrel_ref
  !Reference parameters for non-dimensional problem
  REAL(8):: L_ref, v_ref, ti_ref, Vol_ref, EF_ref, Volt_ref, B_ref

END MODULE moduleRefParam

