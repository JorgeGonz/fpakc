!Problems of the case
MODULE moduleCaseParam
  !Final and initial iterations
  INTEGER:: tFinal, tInitial = 0
  ! Global index of current iteration
  INTEGER:: timeStep
  ! Time step for all species
  REAL(8), ALLOCATABLE:: tau(:)
  ! Minimum time step
  REAL(8):: tauMin
  ! Time step for Monte-Carlo Collisions
  REAL(8):: tauColl

END MODULE moduleCaseParam

