!Physical and mathematical constants
MODULE moduleConstParam
  IMPLICIT NONE

  PUBLIC

  REAL(8), PARAMETER:: PI  = 4.D0*DATAN(1.D0) !number pi
  REAL(8), PARAMETER:: PI2 = 2.D0*PI !2*pi
  REAL(8), PARAMETER:: PI4 = 4.D0*PI !4*pi
  REAL(8), PARAMETER:: PI8 = 8.D0*PI !8*pi
  REAL(8), PARAMETER:: sccm2atomPerS = 4.5D17 !sccm to atom s^-1
  REAL(8), PARAMETER:: qe = 1.60217662D-19 !Elementary charge
  REAL(8), PARAMETER:: kb = 1.38064852D-23 !Boltzmann constants SI
  REAL(8), PARAMETER:: eV2J = qe !Electron volt to Joule conversion
  REAL(8), PARAMETER:: eps_0 = 8.8542D-12 !Epsilon_0

END MODULE moduleConstParam

