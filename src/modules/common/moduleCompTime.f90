!Information to calculate computation time
MODULE moduleCompTime
  IMPLICIT NONE

  PUBLIC

  REAL(8):: tStep    = 0.D0
  REAL(8):: tPush    = 0.D0
  REAL(8):: tReset   = 0.D0
  REAL(8):: tColl    = 0.D0
  REAL(8):: tCoul    = 0.D0
  REAL(8):: tWeight  = 0.D0
  REAL(8):: tEMField = 0.D0

END MODULE moduleCompTime

