!moduleErrors: Manages the different type of errors the program can produce.
!              By errors we understand critical errors (that stop the program),
!              warnings (that only output a message with a WARNING tag),
!              o verbose outputs that can be used for debugging porpouse.
MODULE moduleErrors
  CONTAINS
    SUBROUTINE criticalError(msg, pgr)
      IMPLICIT NONE

      CHARACTER(*), INTENT(in):: msg, pgr
      CHARACTER(:), ALLOCATABLE:: errorMsg

      errorMsg = "CRITICAL error in " // pgr // " with message:" // NEW_LINE('A') //  msg

      ERROR STOP errorMsg

    END SUBROUTINE criticalError

    SUBROUTINE warningError(msg)
      IMPLICIT NONE

      CHARACTER(*), INTENT(in):: msg
      CHARACTER(:), ALLOCATABLE:: errorMsg

      errorMsg  = "WARNING: " // msg

      WRITE (*, '(A)') errorMsg

    END SUBROUTINE warningError

    SUBROUTINE verboseError(msg)
      IMPLICIT NONE

      CHARACTER(*), INTENT(in):: msg
      CHARACTER(:), ALLOCATABLE:: errorMsg

      errorMsg  = msg

      WRITE (*, '(A)') errorMsg

    END SUBROUTINE verboseError

END MODULE moduleErrors
