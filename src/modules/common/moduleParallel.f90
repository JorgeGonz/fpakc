MODULE moduleParallel
  IMPLICIT NONE
  
  TYPE openMP_type
    INTEGER:: nThreads

  END TYPE

  TYPE(openMP_type):: openMP

  CONTAINS
    SUBROUTINE initParallel()
      IMPLICIT NONE

      EXTERNAL:: OMP_SET_NUM_THREADS

      !Starts threads for OpenMP parallelization
      CALL OMP_SET_NUM_THREADS(openMP%nThreads)

    END SUBROUTINE initParallel

END MODULE moduleParallel
