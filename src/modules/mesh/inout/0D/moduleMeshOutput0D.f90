MODULE moduleMeshOutput0D

  CONTAINS
    SUBROUTINE printOutput0D(self)
      USE moduleMesh
      USE moduleRefParam
      USE moduleSpecies
      USE moduleOutput
      USE moduleCaseParam, ONLY: timeStep
      IMPLICIT NONE

      CLASS(meshParticles), INTENT(in):: self
      INTEGER:: i
      TYPE(outputFormat):: output
      CHARACTER(:), ALLOCATABLE:: fileName

      DO i = 1, nSpecies
        fileName='OUTPUT_' // species(i)%obj%name // '.dat'
        IF (timeStep == 0) THEN
          OPEN(20, file = path // folder // '/' // fileName, action = 'write')
          WRITE(20, "(A1, 14X, A5, A20, 40X, A20, 2(A20))") "#","t (s)","density (m^-3)", "velocity (m/s)", &
                                                                        "pressure (Pa)", "temperature (K)"
          WRITE(*, "(6X,A15,A)") "Creating file: ", fileName
          CLOSE(20)

        END IF
        
        OPEN(20, file = path // folder // '/' // fileName, position = 'append', action = 'write')
        CALL calculateOutput(self%nodes(1)%obj%output(i), output, self%nodes(1)%obj%v, species(i)%obj)
        WRITE(20, "(7(ES20.6E3))") REAL(timeStep)*tauMin*ti_ref, output%density,    &
                                                                 output%velocity,   &
                                                                 output%pressure,   &
                                                                 output%temperature
        CLOSE(20)

      END DO

    END SUBROUTINE printOutput0D

    SUBROUTINE printColl0D(self)
      USE moduleMesh
      USE moduleRefParam
      USE moduleCaseParam
      USE moduleCollisions
      USE moduleOutput
      IMPLICIT NONE

      CLASS(meshGeneric), INTENT(in):: self
      CHARACTER(:), ALLOCATABLE:: fileName
      INTEGER:: k

      fileName='OUTPUT_Collisions.dat'
      IF (timeStep == tInitial) THEN
        OPEN(20, file = path // folder // '/' // fileName, action = 'write')
        WRITE(20, "(A1, 14X, A5, A20)") "#","t (s)","collisions"
        WRITE(*, "(6X,A15,A)") "Creating file: ", fileName
        CLOSE(20)

      END IF
      
      OPEN(20, file = path // folder // '/' // fileName, position = 'append', action = 'write')
      WRITE(20, "(ES20.6E3, 10I20)") REAL(timeStep)*tauMin*ti_ref, (self%cells(1)%obj%tallyColl(k)%tally, k=1,nCollPairs)
      CLOSE(20)

    END SUBROUTINE printColl0D

    SUBROUTINE printEM0D(self)
      USE moduleMesh
      USE moduleRefParam
      USE moduleCaseParam
      USE moduleOutput
      IMPLICIT NONE

      CLASS(meshParticles), INTENT(in):: self

    END SUBROUTINE printEM0D

END MODULE moduleMeshOutput0D
