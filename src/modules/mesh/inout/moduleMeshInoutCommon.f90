MODULE moduleMeshInoutCommon

  CHARACTER(LEN=4):: prefix = 'Step'

  CONTAINS
    PURE FUNCTION formatFileName(prefix, suffix, extension, timeStep) RESULT(fileName)
      USE moduleOutput
      IMPLICIT NONE

      CHARACTER(*), INTENT(in):: prefix, suffix, extension
      INTEGER, INTENT(in), OPTIONAL:: timeStep
      CHARACTER (LEN=iterationDigits):: tString
      CHARACTER(:), ALLOCATABLE:: fileName

      IF (PRESENT(timeStep)) THEN
        WRITE(tString, iterationFormat) timeStep
        fileName = prefix //  '_' // tString // '_' // suffix // '.' // extension

      ELSE
        fileName = prefix //  '_' // suffix // '.' // extension

      END IF

    END FUNCTION formatFileName


END MODULE moduleMeshInoutCommon
