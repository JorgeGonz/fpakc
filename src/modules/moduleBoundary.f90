MODULE moduleBoundary
  USE moduleTable
  USE moduleSpecies

  !Generic type for boundaries
  TYPE, PUBLIC:: boundaryGeneric
    CONTAINS

  END TYPE boundaryGeneric

  !Reflecting boundary
  TYPE, PUBLIC, EXTENDS(boundaryGeneric):: boundaryReflection
    CONTAINS

  END TYPE boundaryReflection

  !Absorption boundary
  TYPE, PUBLIC, EXTENDS(boundaryGeneric):: boundaryAbsorption
    CONTAINS

  END TYPE boundaryAbsorption

  !Transparent boundary
  TYPE, PUBLIC, EXTENDS(boundaryGeneric):: boundaryTransparent
    CONTAINS

  END TYPE boundaryTransparent

  !Wall Temperature boundary
  TYPE, PUBLIC, EXTENDS(boundaryGeneric):: boundaryWallTemperature
    !Thermal velocity of the wall: square root(Wall temperature X specific heat)
    REAL(8):: vTh
    CONTAINS

  END TYPE boundaryWallTemperature

  !Ionization boundary
  TYPE, PUBLIC, EXTENDS(boundaryGeneric):: boundaryIonization
    REAL(8):: m0, n0, v0(1:3), vTh !Properties of background neutrals.
    CLASS(speciesGeneric), POINTER:: species !Ion species
    CLASS(speciesCharged), POINTER:: electronSecondary !Pointer to species considerer as secondary electron
    TYPE(table1D):: crossSection
    REAL(8):: effectiveTime
    REAL(8):: eThreshold
    REAL(8):: deltaV
    CONTAINS

  END TYPE boundaryIonization

  !Symmetry axis
  TYPE, PUBLIC, EXTENDS(boundaryGeneric):: boundaryAxis
    CONTAINS

  END TYPE boundaryAxis

  !Wrapper for boundary types (one per species)
  TYPE:: bTypesCont
    CLASS(boundaryGeneric), ALLOCATABLE:: obj
    CONTAINS

  END TYPE bTypesCont

  !Wrapper for boundary conditions
  TYPE:: boundaryCont
    INTEGER:: n = 0
    CHARACTER(:), ALLOCATABLE:: name 
    INTEGER:: physicalSurface = 0 !Physical surface as defined in the mesh file
    CLASS(bTypesCont), ALLOCATABLE:: bTypes(:) !Array for boundary per species
    CONTAINS

  END TYPE boundaryCont

  !Number of boundaries
  INTEGER:: nBoundary = 0
  !Array for boundaries
  TYPE(boundaryCont), ALLOCATABLE, TARGET:: boundary(:)

  CONTAINS
    FUNCTION getBoundaryId(physicalSurface) RESULT(id)
      IMPLICIT NONE

      INTEGER:: physicalSurface
      INTEGER:: id
      INTEGER:: i

      id = 0
      DO i = 1, nBoundary
        IF (physicalSurface == boundary(i)%physicalSurface) id = boundary(i)%n

      END DO

    END FUNCTION getBoundaryId

    SUBROUTINE initWallTemperature(boundary, T, c)
      USE moduleRefParam
      IMPLICIT NONE

      CLASS(boundaryGeneric), ALLOCATABLE, INTENT(out):: boundary
      REAL(8), INTENT(in):: T, c !Wall temperature and specific heat
      REAL(8):: vTh

      vTh = DSQRT(c * T) / v_ref
      boundary = boundaryWallTemperature(vTh = vTh)

    END SUBROUTINE initWallTemperature

    SUBROUTINE initIonization(boundary, me, m0, n0, v0, T0, ion, effTime, crossSection, eThreshold, electronSecondary)
      USE moduleRefParam
      USE moduleSpecies
      USE moduleCaseParam
      USE moduleConstParam
      USE moduleErrors
      IMPLICIT NONE

      CLASS(boundaryGeneric), ALLOCATABLE, INTENT(out):: boundary
      REAL(8), INTENT(in):: me !Electron mass
      REAL(8), INTENT(in):: m0, n0, v0(1:3), T0 !Neutral properties
      INTEGER, INTENT(in):: ion
      INTEGER, OPTIONAL, INTENT(in):: electronSecondary
      REAL(8):: effTime
      CHARACTER(:), ALLOCATABLE, INTENT(in):: crossSection
      REAL(8), INTENT(in):: eThreshold

      ALLOCATE(boundaryIonization:: boundary)

      SELECT TYPE(boundary)
      TYPE IS(boundaryIonization)
        boundary%m0 = m0 / m_ref
        boundary%n0 = n0 * Vol_ref
        boundary%v0 = v0 / v_ref
        boundary%vTh = DSQRT(kb*T0/m0)/v_ref
        boundary%species => species(ion)%obj
        IF (PRESENT(electronSecondary)) THEN
          SELECT TYPE(sp => species(electronSecondary)%obj)
          TYPE IS(speciesCharged)
            boundary%electronSecondary => sp

          CLASS DEFAULT
            CALL criticalError("Species " // sp%name // " chosen for " // &
                               "secondary electron is not a charged species", 'initIonization')

          END SELECT

        ELSE
          boundary%electronSecondary => NULL()

        END IF
        boundary%effectiveTime = effTime / ti_ref
        CALL boundary%crossSection%init(crossSection)
        CALL boundary%crossSection%convert(eV2J/(m_ref*v_ref**2), 1.D0/L_ref**2)
        boundary%eThreshold = eThreshold*eV2J/(m_ref*v_ref**2)
        boundary%deltaV = DSQRT(boundary%eThreshold/me)

      END SELECT

    END SUBROUTINE initIonization

END MODULE moduleBoundary
