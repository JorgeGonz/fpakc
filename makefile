# set folders
TOPDIR =  $(PWD)# top directory
MODDIR := $(TOPDIR)/mod# module folder
OBJDIR := $(TOPDIR)/obj# object folder
SRCDIR := $(TOPDIR)/src# source folder
# compiler
# gfortran:
FC := gfortran
JSONDIR := $(TOPDIR)/json-fortran/build-gfortran
# ifort:
# FC := ifort
# JSONDIR := $(TOPDIR)/json-fortran-8.2.0/build-ifort
JSONLIB := $(JSONDIR)/lib/libjsonfortran.a
JSONINC := $(JSONDIR)/include

INCDIR :=
INCDIR += $(JSONINC)

# compiler flags
# gfortran:
FCFLAGS := -fopenmp -Ofast -J $(MODDIR) -I $(INCDIR) -Wall -march=native -g
# ifort:
# FCFLAGS := -qopenmp -Ofast -xHost -module $(MODDIR) -I $(INCDIR) -warn all -parallel -free -g

#Output file
OUTPUT = fpakc

export

all: $(OUTPUT)

$(OUTPUT):  src.o 

src.o:
	@mkdir -p $(MODDIR)
	@mkdir -p $(OBJDIR)
	$(MAKE) -C src $(OUTPUT)

clean:
	rm -f $(OUTPUT)
	rm -f $(MODDIR)/*.mod
	rm -f $(MODDIR)/*.smod
	rm -f $(OBJDIR)/*.o

