# Introduction
Welcome to **fpakc** (Finite element PArticle Kinetic Code), a modern object oriented Fortran open-source code for particle simulations of plasma and gases. This code works by simulating charged and neutral particles, following their trajectories, collisions and boundary conditions imposed by the user.

One of our aims is to make a code easy to maintain as well as easy to use by a variety of reserchers and students.

This code is currenlty in very early steps of development.

The code aims to be easy to maintain and easy to use, allowing its application from complex problems to easy examples that can be used, for example, as teaching exercises.

Parallelization techniques such as OpenMP, MPI will be used to distribute the cpu load. We aim to make fpakc GPU compatible in the future.

The codefpakc makes use of finite elements to generate meshes in complex geometries. Particle properties are deposited in the nodes and cells of the mesh. The electromagnetic field, with the boundary conditions imposed by the user, is solved also in this mesh.

# User Manual

You will find the user manual in the *doc/user-manual/* folder. It contains information about the installation of fpkac, how to run the software, the example cases distributed with the code and how the code operates.

# Installation

Once you have been added to the fpakc gitlab repository, and have added an ssh key to your account, you will be able to clone the repository using:
```
git clone git@gitlab.com:<YourGitLabUsername>/fpakc.git
```
in which you have to substitute `<YourGitLabUsername>` for your gitlab username.

Go into the new downloaded directory with
```
cd fpakc
```
Clone the json-fortran repository with:
```
git clone https://github.com/jacobwilliams/json-fortran.git
```
This will create the  `json-fortran-8.2.0` directory. Go into this new folder and create a new `build-gfortran` directory, enter it and execute:
```
cmake ../
```
Once it finish, just compile the json-fortran library with
```
make
```
This creates the `include` and `lib` folders that contain the required files for fpakc compilation.

Go back to the fpakc root folder and execute
```
make
```
to compile the code. If everything is correct, an executable named `fpakc` will be generated.
