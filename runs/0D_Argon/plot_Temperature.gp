reset
set macros
term = 'postscript eps color enhanced'
font = 'font "CMUSerif-Roman,20"'
linew = 'lw 2.0'
times='"{/Symbol \264}"'
size_x=8.5
size_y=5.2

tmar=0.998
bmar=0.145

lmarLabel = 0.070

lmar=lmarLabel+0.085
rmar=0.97

space_x=(rmar-lmar)

xt_off_0=0.5
yt_off_0=0.6

set xtics nomirror offset 0.0,xt_off_0
set mxtics 2

set ytics nomirror offset yt_off_0,0.0
set mytics 2

set pointsize 1.5

set style line 1  pt 4              lc rgb "#B50427" #Squares   red
set style line 2  pt 6              lc rgb "#3B4CC1" #Circles   blue
set style line 3  pt 1              lc rgb "#2CA02C" #Crosses   green
set style line 4  pt 2              lc rgb "#FE7F0E" #Exes      orange
set style line 5  pt 8              lc rgb "#D6696B" #Triangles light red
set style line 10       lt 1 lw 2.0 lc rgb "black"   #Black solid line

set terminal @term size size_x cm, size_y cm @linew @font

set output "comp_temp.eps"

#files
filename1 = "OUTPUT_Argon.dat"
filename2 = "OUTPUT_Argon+.dat"

folder = 'output/'

set lmargin at screen lmar 
set rmargin at screen rmar

set xrange [-0.01:1.01]
set xtics 0.2
set xlabel "Time (s)" offset 0.0,1.2

set yrange [250:3550]
set format y "%3.0f"
set ytics 500
set ylabel "Temperature (K)" offset 1.4,0.0

set key width 0.5 height 0.1 spacing 1.3 samplen 0.2 box opaque font ",16"
set key at graph 0.95, graph 0.9 right top

plot folder.filename1 u ($1):($7)                                          t "Ar"     ls 1 with lines, \
     folder.filename2 u ($1):($7)                                          t "Ar^{+}" ls 2 with lines

