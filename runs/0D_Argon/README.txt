Example of 0D geometry.

Mostly used to test collision processes.

This example includes Ar and Ar+ with self-collisions for Ar and elastic collisions for AR-Ar+.

Different starting temperatures for each species that end up in equilibrium.

The gnuplpot script 'plot_Temperature.gp' generates a plot of the species temperatures.

The result is provided in the output folder to compare if modifications to the code are made.
