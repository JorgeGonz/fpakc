reset
set terminal qt enhanced 1 persist size 1600, 600 font "Times ,10"

set style line 1  pt 4              lc rgb "#B50427" #Squares   red
set style line 2  pt 6              lc rgb "#3B4CC1" #Circles   blue
set style line 3  pt 1              lc rgb "#2CA02C" #Crosses   green
set style line 4  pt 2              lc rgb "#FE7F0E" #Exes      orange
set style line 5  pt 8              lc rgb "#D6696B" #Triangles light red
set style line 10       lt 1 lw 2.0 lc rgb "black"   #Black solid line

set style data histogram
set style histogram rowstacked title offset 0,-1
set style fill solid border -1
set boxwidth 0.50

set key box opaque

set yrange [0:100]
set ylabel "CPU Time (%)"
set xrange [0:]


plot "cpuTime.dat" u ($4/$3*100) t "Push"       ls 1, \
     ""            u ($5/$3*100) t "Reset"      ls 2, \
	 ""            u ($6/$3*100) t "Collisions" ls 3, \
	 ""            u ($7/$3*100) t "Weighting"  ls 4
	 
reset
set terminal qt enhanced 2 size 800, 600 font "Times ,10"

set style line 1  pt 4              lc rgb "#B50427" #Squares   red
set style line 2  pt 6              lc rgb "#3B4CC1" #Circles   blue
set style line 3  pt 1              lc rgb "#2CA02C" #Crosses   green
set style line 4  pt 2              lc rgb "#FE7F0E" #Exes      orange
set style line 5  pt 8              lc rgb "#D6696B" #Triangles light red
set style line 10       lt 1 lw 2.0 lc rgb "black"   #Black solid line

set key box opaque
set pointsize 1.5

set ylabel "Time per particle ({/Symbol m}s)"
set xrange [0:]
set xlabel "Iteration"


plot "cpuTime.dat" u 1:(1e6*$4/$2) t "Push"        ls 1, \
     ""            u 1:(1e6*$5/$2) t "Reset"       ls 2, \
	 ""            u 1:(1e6*$6/$2) t "Collisions"  ls 3, \
	 ""            u 1:(1e6*$7/$2) t "Weighting"   ls 4
