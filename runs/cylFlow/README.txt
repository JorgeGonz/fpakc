Neutral flow around a cylinder with elastic collisions.

Case with the same mesh for particle weighting and collisions and another case using the dual mesh mode, with a more refined mesh around the cylinder.
This demostrates the capacity of fpakc to work with independent meshes for particles and collisions.

CPU time is outputed.
Gnuplot scripts for plotting the CPU time are provided.
