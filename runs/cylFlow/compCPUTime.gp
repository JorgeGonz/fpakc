## Total time per iteration
set terminal qt enhanced 1 persist size 1600, 1000 font "Times ,10"

set style line 1  pt 4              lc rgb "#B50427" #Squares   red
set style line 2  pt 6              lc rgb "#3B4CC1" #Circles   blue
set style line 3  pt 1              lc rgb "#2CA02C" #Crosses   green
set style line 4  pt 2              lc rgb "#FE7F0E" #Exes      orange
set style line 5  pt 8              lc rgb "#D6696B" #Triangles light red
set style line 10       lt 1 lw 2.0 lc rgb "black"   #Black solid line

#Name and folder 1 for comparison (include / at the end of the folder)
name1   = ""
folder1 = ""
#Name and folder 2 for comparison (include / at the end of the folder)
name2   = ""
folder2 = ""
#Name and folder 3 for comparison (include / at the end of the folder)
name3   = ""
folder3 = ""
#Name and folder 4 for comparison (include / at the end of the folder)
name4   = ""
folder4 = ""

set key box opaque
set pointsize 1.5

set ylabel "Time per particle (ms)"
set xrange [0:500]
set xlabel "Iteration"


set multiplot layout 2,3
set title "Total"
plot folder1."cpuTime.dat" u 1:(1e3*$3) t name1       ls 1, \
     folder2."cpuTime.dat" u 1:(1e3*$3) t name2       ls 2, \
     folder3."cpuTime.dat" u 1:(1e3*$3) t name3       ls 3, \
     folder4."cpuTime.dat" u 1:(1e3*$3) t name4       ls 4
	 
set title "Push"
plot folder1."cpuTime.dat" u 1:(1e3*$4) t name1       ls 1, \
     folder2."cpuTime.dat" u 1:(1e3*$4) t name2       ls 2, \
     folder3."cpuTime.dat" u 1:(1e3*$4) t name3       ls 3, \
     folder4."cpuTime.dat" u 1:(1e3*$4) t name4       ls 4
	 
set title "Reset"
plot folder1."cpuTime.dat" u 1:(1e3*$5) t name1       ls 1, \
     folder2."cpuTime.dat" u 1:(1e3*$5) t name2       ls 2, \
     folder3."cpuTime.dat" u 1:(1e3*$5) t name3       ls 3, \
     folder4."cpuTime.dat" u 1:(1e3*$5) t name4       ls 4
	 
set title "Collisions"
plot folder1."cpuTime.dat" u 1:(1e3*$6) t name1       ls 1, \
     folder2."cpuTime.dat" u 1:(1e3*$6) t name2       ls 2, \
     folder3."cpuTime.dat" u 1:(1e3*$6) t name3       ls 3, \
     folder4."cpuTime.dat" u 1:(1e3*$6) t name4       ls 4
	 
set title "Scattering"
plot folder1."cpuTime.dat" u 1:(1e3*$7) t name1       ls 1, \
     folder2."cpuTime.dat" u 1:(1e3*$7) t name2       ls 2, \
     folder3."cpuTime.dat" u 1:(1e3*$7) t name3       ls 3, \
     folder4."cpuTime.dat" u 1:(1e3*$7) t name4       ls 4
	 
set title "Num. particles"
plot folder1."cpuTime.dat" u 1:($2) t name1       ls 1, \
     folder2."cpuTime.dat" u 1:($2) t name2       ls 2, \
     folder3."cpuTime.dat" u 1:($2) t name3       ls 3, \
     folder4."cpuTime.dat" u 1:($2) t name4       ls 4

unset multiplot
	 
## Time per particle
# set terminal qt enhanced 2 persist size 1600, 1000 font "Times ,10"
# 
# set style line 1  pt 4              lc rgb "#B50427" #Squares   red
# set style line 2  pt 6              lc rgb "#3B4CC1" #Circles   blue
# set style line 3  pt 1              lc rgb "#2CA02C" #Crosses   green
# set style line 4  pt 2              lc rgb "#FE7F0E" #Exes      orange
# set style line 5  pt 8              lc rgb "#D6696B" #Triangles light red
# set style line 10       lt 1 lw 2.0 lc rgb "black"   #Black solid line
# 
# set key box opaque
# set pointsize 1.5
# 
# set ylabel "Time per particle (micros)"
# set xrange [0:500]
# set xlabel "Iteration"
# 
# 
# set multiplot layout 2,3
# set title "Total"
# plot folder1."cpuTime.dat" u 1:(1e6*$3/$2) t name1       ls 1, \
#      folder2."cpuTime.dat" u 1:(1e6*$3/$2) t name2       ls 2, \
#      folder3."cpuTime.dat" u 1:(1e6*$3/$2) t name3       ls 3, \
#      folder4."cpuTime.dat" u 1:(1e6*$3/$2) t name4       ls 4
# 	 
# set title "Push"
# plot folder1."cpuTime.dat" u 1:(1e6*$4/$2) t name1       ls 1, \
#      folder2."cpuTime.dat" u 1:(1e6*$4/$2) t name2       ls 2, \
#      folder3."cpuTime.dat" u 1:(1e6*$4/$2) t name3       ls 3, \
#      folder4."cpuTime.dat" u 1:(1e6*$4/$2) t name4       ls 4
# 	 
# set title "Reset"
# plot folder1."cpuTime.dat" u 1:(1e6*$5/$2) t name1       ls 1, \
#      folder2."cpuTime.dat" u 1:(1e6*$5/$2) t name2       ls 2, \
#      folder3."cpuTime.dat" u 1:(1e6*$5/$2) t name3       ls 3, \
#      folder4."cpuTime.dat" u 1:(1e6*$5/$2) t name4       ls 4
# 	 
# set title "Collisions"
# plot folder1."cpuTime.dat" u 1:(1e6*$6/$2) t name1       ls 1, \
#      folder2."cpuTime.dat" u 1:(1e6*$6/$2) t name2       ls 2, \
#      folder3."cpuTime.dat" u 1:(1e6*$6/$2) t name3       ls 3, \
#      folder4."cpuTime.dat" u 1:(1e6*$6/$2) t name4       ls 4
# 	 
# set title "Scattering"
# plot folder1."cpuTime.dat" u 1:(1e6*$7/$2) t name1       ls 1, \
#      folder2."cpuTime.dat" u 1:(1e6*$7/$2) t name2       ls 2, \
#      folder3."cpuTime.dat" u 1:(1e6*$7/$2) t name3       ls 3, \
#      folder4."cpuTime.dat" u 1:(1e6*$7/$2) t name4       ls 4
# 
# unset multiplot
