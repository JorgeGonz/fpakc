cl__1 = 1;
cyl_h = 0.005;
cyl_l = 0.02;
cyl_s = 0.03;
cyl_e = cyl_s + cyl_l;
dom_h = 0.03;
dom_l = 0.07;

Lcell = 0.001;

Point(1)  = {0,     0,     0, cl__1};
Point(2)  = {cyl_s, 0,     0, cl__1};
Point(3)  = {cyl_s, cyl_h, 0, cl__1};
Point(4)  = {cyl_e, cyl_h, 0, cl__1};
Point(5)  = {cyl_e, 0,     0, cl__1};
Point(6)  = {dom_l, 0,     0, cl__1};
Point(7)  = {dom_l, cyl_h, 0, cl__1};
Point(8)  = {dom_l, dom_h, 0, cl__1};
Point(9)  = {cyl_e, dom_h, 0, cl__1};
Point(10) = {cyl_s, dom_h, 0, cl__1};
Point(11) = {0,     dom_h, 0, cl__1};
Point(12) = {0,     cyl_h, 0, cl__1};

Line(1)  = {1,  2};
Line(2)  = {2,  3};
Line(3)  = {3,  4};
Line(4)  = {4,  5};
Line(5)  = {5,  6};
Line(6)  = {6,  7};
Line(7)  = {7,  8};
Line(8)  = {8,  9};
Line(9)  = {9,  10};
Line(10) = {10, 11};
Line(11) = {11, 12};
Line(12) = {12, 1};
Line(13) = {12, 3};
Line(14) = {4,  7};
Line(15) = {4,  9};
Line(16) = {10, 3};

Line Loop(1) = {1, 2, -13, 12};
Plane Surface(1) = {1};
Line Loop(2) = {13, -16, 10, 11};
Plane Surface(2) = {2};
Line Loop(3) = {3, 15, 9, 16};
Plane Surface(3) = {3};
Line Loop(4) = {5, 6, -14, 4};
Plane Surface(4) = {4};
Line Loop(5) = {14, 7, 8, -15};
Plane Surface(5) = {5};

Physical Line(1) = {12, 11};
Physical Line(2) = {10, 9, 8};
Physical Line(3) = {7, 6};
Physical Line(4) = {2, 3, 4};
Physical Line(5) = {1, 5};

Physical Surface(1) = {1};
Physical Surface(2) = {2};
Physical Surface(3) = {3};
Physical Surface(4) = {4};
Physical Surface(5) = {5};

Transfinite Line {12, 2, 4, 6} = cyl_h/Lcell + 1 Using Progression 1;
Transfinite Line {1, 13, 10} = cyl_s/Lcell + 1 Using Progression 1;
Transfinite Line {11, 16, 15, 7} = (dom_h - cyl_h)/Lcell + 1 Using Progression 1;
Transfinite Line {3, 9} = cyl_l/Lcell + 1 Using Progression 1;
Transfinite Line {5, 14, 8} = (dom_l - cyl_e)/Lcell + 1 Using Progression 1;

Transfinite Surface{1};
Recombine Surface {1};
Transfinite Surface{2};
Recombine Surface {2};
Transfinite Surface{3};
Recombine Surface {3};
Transfinite Surface{4};
Recombine Surface {4};
Transfinite Surface{5};
Recombine Surface {5};
