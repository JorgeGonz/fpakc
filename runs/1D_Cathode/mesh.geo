Lcell = 4e-5;

x0 = 0.001;
xf = x0 + 50.0*Lcell;

Point(1) = {x0, 0, 0, 1};
Point(2) = {xf, 0, 0, 1};

Line(1) = {1, 2};

Physical Point(1) = {1};
Physical Point(2) = {2};

Physical Line(1) = {1};

Transfinite Line {1} = (xf-x0)/Lcell + 1 Using Progression 1;
