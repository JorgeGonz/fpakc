This case is useful to explain how fpakc can work with different geometries (Cartisian and Radial in this case) using very similar input files and the same mesh.

From the position to the left, electrons  are emitted at a constant rate.
The electric potential at the left is fixed at 0.

Depending on the geometry (which affects the symmetry) different distributions of the electron density and electric potential are achieved.

The last iteration obtained for these cases is in the output folder.
