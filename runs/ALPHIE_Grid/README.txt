Alphie grid system.

The file mesh.geo can be easily modified to generate new grids to test different configurations.

Two cases are provided:
  Clasical case in which ions and electrons are input from the ionization chamber.
  Ionization case in which ion-electron pairs are generated due to the influx of electrons using the Ionization boundary condition.
